var express = require("express");
var router = express.Router();
var Campground = require("../models/campground");
var middleWare = require("../middleware/index.js");

router.get("/", function(req, res){
  Campground.find({}, function(err, allCampgrounds){
    if(err){
      console.log(err);
    } else {
        res.render("campgrounds/index", {campgrounds: allCampgrounds});
    }
  })
});
router.post("/", middleWare.isLoggedIn, function(req, res){
  var newCamp = {
    name: req.body.name,   // create new object
    image: req.body.image,
    description: req.body.description,
    price: req.body.price,
    author: {
      id: req.user._id,
      username: req.user.username
    }
  }
  Campground.create(newCamp,function(err, campground){
     if(err){
       console.log(err);
     } else{
        req.flash("sucess", "Successfully created a new post!")
        res.redirect("/campgrounds");
       }
   });
});

router.get("/new", middleWare.isLoggedIn, function(req,res){
    res.render("campgrounds/new");
});
router.get("/:id", function(req, res){
  Campground.findById(req.params.id).populate("comments").exec(function(err, foundCampground){
    if(err){
      console.log(err);
    } else{
      res.render("campgrounds/show", {campground: foundCampground});
    }
  });
});

router.get("/:id/edit", middleWare.checkCampgroundOwnership, function(req,res){

  Campground.findById(req.params.id, function(err, foundCampground){
      if(err){
        req.flash("error", "Campground do not exists!");
      } else{
        res.render("campgrounds/edit", {campground: foundCampground});
      }
    });
});
router.put("/:id", middleWare.checkCampgroundOwnership, function(req,res){
  // req.body.campground.body = req.sanitize(req.body.campground.body);
  Campground.findByIdAndUpdate(req.params.id, req.body.campground, function(err, updatedcampground){
    if(err){
      req.flash("error", "You do not have the premissions to do that");
    } else {
      req.flash("success", "Successfully updated campground information");
      res.redirect("/campgrounds/" + req.params.id);
    }
  });
});
router.delete("/:id", middleWare.checkCampgroundOwnership, function(req,res){
  Campground.findByIdAndRemove(req.params.id, function(err){
    if(err){
      req.flash("error", "You do not have the premission to do that");
      res.redirect("/campgrounds");
    } else {
      req.flash("success", "Campground deleted successfully");
      res.redirect("/campgrounds");
    }
  });
});
module.exports = router;
